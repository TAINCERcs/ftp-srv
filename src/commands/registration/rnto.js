const Promise = require('bluebird');
const path = require('path');

module.exports = {
  directive: 'RNTO',
  handler: function ({log, command} = {}) {
    if (!this.renameFrom) return this.reply(503);

    if (!this.fs) return this.reply(550, 'File system not instantiated');
    if (!this.fs.rename) return this.reply(402, 'Not supported by file system');

    const from = this.renameFrom;
    const to = command.arg;

    // TODO: Beschränkung nur auf Ordner Anwenden.
    return this.reply(553, 'SALS-Server: Renaming Files/Folders is not allowed');

    Promise.try(() => this.fs.currentDirectory())
    .then((pwd) => {
      const fileStat = false;
      if (fileStat.isDirectory()) {
        this.emit('RNTO', 'Renaming Folders is not allowed');
        return this.reply(553, 'Renaming Folders is not allowed');
      }

      return Promise.try(() => this.fs.rename(from, to))
      .then(() => {
        return this.reply(250);
      })
      .tap(() => this.emit(
        'RNTO',
        null,
        path.join(config.FTP.rootDirectory, pwd, from),
        path.join(config.FTP.rootDirectory, pwd, to)
      ))
      .catch((err) => {
        log.error(err);
        this.emit('RNTO', err);
        return this.reply(550, err.message);
      })
      .finally(() => {
        delete this.renameFrom;
      });
    });

  },
  syntax: '{{cmd}} <name>',
  description: 'Rename to'
};
