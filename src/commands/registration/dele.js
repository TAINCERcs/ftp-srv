const Promise = require('bluebird');
const path = require('path');

module.exports = {
  directive: 'DELE',
  handler: function ({log, command} = {}) {
    if (!this.fs) return this.reply(550, 'File system not instantiated');
    if (!this.fs.delete) return this.reply(402, 'Not supported by file system');

    Promise.try(() => this.fs.currentDirectory())
    .then((pwd) => {
      return Promise.try(() => this.fs.delete(command.arg))
      .then(() => {
        if (command.directive === 'DELE') {
          this.emit('DELE', null, path.join(pwd, command.arg));
        }
        return this.reply(250);
      })
      .catch((err) => {
        log.error(err);
        this.emit('DELE', err, null);
        return this.reply(550, err.message);
      });
    });
  },
  syntax: '{{cmd}} <path>',
  description: 'Delete file'
};
